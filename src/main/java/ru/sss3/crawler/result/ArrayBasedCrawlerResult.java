package ru.sss3.crawler.result;

import ru.sss3.util.IdHolder;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author sss3 (RDYakbarov)
 */
public class ArrayBasedCrawlerResult implements CrawlerResult {

    private final IdHolder idHolder;
    private final AtomicInteger[][] matrix;
    private Map<Integer, String> reverse = null;

    ArrayBasedCrawlerResult(int matrixSize) {
        this.idHolder = new IdHolder(matrixSize);
        this.matrix = new AtomicInteger[matrixSize][matrixSize];
        initMatrix();
    }

    @Override
    public boolean addLink(String from, String to) {
        if ((!idHolder.contains(from) || !idHolder.contains(to)) && !idHolder.hasNext()) {
            return false;
        }

        final int fromIndex = idHolder.getIndex(from);
        final int toIndex = idHolder.getIndex(to);
        matrix[fromIndex][toIndex].getAndIncrement();
        return true;
    }

    @Override
    public Collection<String> allLinks() {
        return idHolder.ids();
    }

    @Override
    public Collection<String> in(String page) {
        if (reverse == null) {
            reverse = idHolder.reverse();
        }

        final int col = idHolder.getIndex(page);
        final List<String> result = new ArrayList<>();

        for (int i = 0; i < matrix[col].length; i++) {
            if (matrix[i][col].get() > 0 && i != col) {
                result.add(reverse.get(i));
            }
        }

        return result;
    }

    @Override
    public Collection<String> out(String page) {
        if (reverse == null) {
            reverse = idHolder.reverse();
        }

        final int row = idHolder.getIndex(page);
        final List<String> result = new ArrayList<>();

        for (int i = 0; i < matrix[row].length; i++) {
            if (matrix[row][i].get() > 0 && i != row) {
                result.add(reverse.get(i));
            }
        }

        return result;
    }

    @Override
    public void printAsMatrix(PrintWriter printWriter) {
        idHolder.ids().forEach(id -> printWriter.println(idHolder.getIndex(id) + " : " + id));
        printWriter.println();
        printWriter.flush();
        for (AtomicInteger[] links : matrix) {
            printWriter.println(Arrays.toString(links));
            printWriter.flush();
        }
        printWriter.println();
        printWriter.flush();
    }

    private void initMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = new AtomicInteger();
            }
        }
    }
}
