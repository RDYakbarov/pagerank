package ru.sss3.crawler;

import ru.sss3.crawler.result.CrawlerResult;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author sss3 (RDYakbarov)
 */
public class MultiThreadCrawler implements Crawler {

    private final ForkJoinPool service;

    MultiThreadCrawler(int threadCount) {
        service = new ForkJoinPool(threadCount);
    }

    @Override
    public CrawlerResult work(String startUrl) {
        final CrawlerResult result = CrawlerResult.newInstance();
        service.invoke(new Worker(result, new ConcurrentHashMap<>(), startUrl));
        service.shutdown();
        return result;
    }

    private static class Worker extends RecursiveAction {

        final CrawlerResult result;
        final Map<String, Boolean> visitUrl;
        final String from;

        private Worker(CrawlerResult result, Map<String, Boolean> visitUrl, String from) {
            this.result = result;
            this.visitUrl = visitUrl;
            this.from = from;
        }


        @Override
        public void compute() {
            addIfAbsent(visitUrl, from, Boolean.TRUE);
            final List<Worker> workers = Crawler.parse(from)
                    .filter(url -> result.addLink(from, url))
                    .filter(u -> addIfAbsent(visitUrl, u, Boolean.TRUE))
                    .map(url -> new Worker(result, visitUrl, url)).collect(Collectors.toList());
            invokeAll(workers);
        }

        static <K, V> boolean addIfAbsent(Map<K, V> map, K key, V value) {
            if (map.containsKey(key)) {
                return false;
            }
            map.put(key, value);
            return true;
        }

    }
}
