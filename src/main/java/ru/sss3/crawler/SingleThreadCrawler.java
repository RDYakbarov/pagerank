package ru.sss3.crawler;

import ru.sss3.crawler.result.CrawlerResult;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sss3 (RDYakbarov)
 */
public class SingleThreadCrawler implements Crawler {

    SingleThreadCrawler() {}

    public CrawlerResult work(String startUrl) {
        final CrawlerResult result = CrawlerResult.newInstance();
        work(startUrl, result, new HashSet<>());
        return result;
    }

    private void work(String from, final CrawlerResult result, final Set<String> visitUrl) {
        visitUrl.add(from);
        Crawler.parse(from)
                .filter(url -> result.addLink(from, url))
                .filter(visitUrl::add)
                .forEach(url -> work(url, result, visitUrl));
    }

}
