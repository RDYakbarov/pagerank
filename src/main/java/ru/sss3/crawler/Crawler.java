package ru.sss3.crawler;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import ru.sss3.Keys;
import ru.sss3.crawler.result.CrawlerResult;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author sss3 (RDYakbarov)
 */
public interface Crawler {

    String ANCHOR = "#";

    /**
     * @param startUrl where to start
     * @return tree of links since startUrl
     */
    CrawlerResult work(String startUrl);

    static Crawler newInstance() {
        final String crawlerThreads = Keys.get(Keys.CRAWLER_THREADS);
        boolean useMulti = StringUtils.isNumeric(crawlerThreads) && Integer.valueOf(crawlerThreads) > 1;
        return useMulti ? new MultiThreadCrawler(Integer.valueOf(crawlerThreads)) : new SingleThreadCrawler();
    }

    static Stream<String> parse(String page) {
        try {
            return Jsoup.connect(page).get()
                    .body()
                    .select("a")
                    .stream()
                    .map(l -> l.attr("abs:href"))
                    .map(l -> l.contains(ANCHOR) ? l.substring(0, l.indexOf(ANCHOR)) : l)
                    .filter(Objects::nonNull).filter(s -> !s.isEmpty());
        } catch (Exception e) {
            return Stream.empty();
        }

    }



}
