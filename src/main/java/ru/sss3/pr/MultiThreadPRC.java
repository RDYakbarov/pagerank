package ru.sss3.pr;

import ru.sss3.crawler.result.CrawlerResult;
import ru.sss3.util.IdHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author sss3 (RDYakbarov)
 */
public class MultiThreadPRC extends BasePRC implements PageRankCalculator {

    private final ExecutorService pool;

    MultiThreadPRC(int threadCount) {
        this.pool = Executors.newFixedThreadPool(threadCount);
    }

    @Override
    public Collection<PageRankData> calculate(CrawlerResult result) {
        final int size = result.allLinks().size();
        double[] pageRanks = init(size);
        final IdHolder idHolder = new IdHolder(size);
        double[] prevRanks;

        do {
            prevRanks = Arrays.copyOf(pageRanks, pageRanks.length);
            final double[] prevRanksFinal = prevRanks;
            try {
                final List<Worker> workers = result.allLinks().stream()
                        .map(l -> new Worker(prevRanksFinal, result, l, idHolder.getIndex(l)))
                        .collect(Collectors.toList());
                final List<Future<PRCData>> futures = pool.invokeAll(workers);
                for (Future<PRCData> future : futures) {
                    final PRCData prcData = future.get();
                    pageRanks[prcData.index] = prcData.rank;
                }
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
            pageRanks = normalize(pageRanks);
        } while (diff(pageRanks, prevRanks) > EPS);
        pool.shutdown();
        double[] finalPageRanks = pageRanks;
        return idHolder.ids().stream()
                .map(id -> new PageRankData(id, finalPageRanks[idHolder.getIndex(id)]))
                .collect(Collectors.toList());
    }

    private static class PRCData {
        final int index;
        final double rank;

        private PRCData(int index, double rank) {
            this.index = index;
            this.rank = rank;
        }
    }

    private static class Worker implements Callable<PRCData> {

        private final double[] prevRanks;
        private final CrawlerResult crawlerResult;
        private final String page;
        private final int index;

        private Worker(double[] prevRanks, CrawlerResult crawlerResult, String page, int index) {
            this.prevRanks = prevRanks;
            this.crawlerResult = crawlerResult;
            this.page = page;
            this.index = index;
        }


        @Override
        public PRCData call() throws Exception {
            final double[] newRank = {(1 - DAMPING_FACTOR) / crawlerResult.allLinks().size()};
            crawlerResult.out(page).forEach(in -> {
                int out = crawlerResult.in(in).size();
                newRank[0] += DAMPING_FACTOR * prevRanks[index] * 1D / (out == 0 ? 1 : out);
            });
            return new PRCData(index, newRank[0]);
        }
    }

}
