package ru.sss3.pr;

import org.apache.commons.lang3.StringUtils;
import ru.sss3.Keys;
import ru.sss3.crawler.result.CrawlerResult;

import java.util.Collection;

/**
 * @author sss3 (RDYakbarov)
 */
public interface PageRankCalculator {

    double DAMPING_FACTOR = 0.85;
    double EPS = 0.000001;

    /**
     * @param result tree links
     * @return page ranks of tree links
     */
    Collection<PageRankData> calculate(CrawlerResult result);

    static PageRankCalculator newInstance() {
        final String property = Keys.get(Keys.PRC_THREADS);
        boolean useMulti = StringUtils.isNumeric(property) && Integer.valueOf(property) > 1;
        return useMulti ? new MultiThreadPRC(Integer.valueOf(property)) : new SingleThreadPRC();
    }

}
