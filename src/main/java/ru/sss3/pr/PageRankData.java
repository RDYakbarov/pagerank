package ru.sss3.pr;

/**
 * @author sss3 (RDYakbarov)
 */
public class PageRankData {

    private final String page;
    private final double rank;

    PageRankData(String page, double rank) {
        this.page = page;
        this.rank = rank;
    }

    public String page() {
        return page;
    }

    public double rank() {
        return rank;
    }

}
