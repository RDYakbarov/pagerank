package ru.sss3.pr;

import ru.sss3.crawler.result.CrawlerResult;
import ru.sss3.util.IdHolder;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author sss3 (RDYakbarov)
 */
public class SingleThreadPRC extends BasePRC implements PageRankCalculator {

    @Override
    public Collection<PageRankData> calculate(final CrawlerResult result) {
        final int size = result.allLinks().size();
        double[] pageRanks = init(size);
        final IdHolder idHolder = new IdHolder(size);
        double[] prevRanks;
        do {
            prevRanks = Arrays.copyOf(pageRanks, pageRanks.length);
            final double[] finalPrevRanks = prevRanks;
            for (String link : result.allLinks()) {
                final double[] newRank = new double[]{(1 - DAMPING_FACTOR) / size};
                final int index = idHolder.getIndex(link);
                result.out(link).forEach(out -> {
                    int in = result.in(out).size();
                    newRank[0] += DAMPING_FACTOR * finalPrevRanks[index] * 1D / (in == 0 ? 1 : in);
                });
                pageRanks[index] = newRank[0];
            }
            pageRanks = normalize(pageRanks);
        } while (diff(pageRanks, prevRanks) > EPS);

        double[] finalPageRanks = pageRanks;
        return idHolder.ids().stream()
                .map(id -> new PageRankData(id, finalPageRanks[idHolder.getIndex(id)]))
                .collect(Collectors.toList());
    }

}
