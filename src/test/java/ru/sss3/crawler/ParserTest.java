package ru.sss3.crawler;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author sss3 (RDYakbarov)
 */
public class ParserTest {


    @Test
    public void withResult() {
        Assert.assertEquals("Invalid result size", Crawler.parse("http://novikov.amikeco.ru/iretrieve.html").count(), 10L);
    }

    @Test
    public void hasntUrlTest() {
        Assert.assertEquals("Invalid result size", Crawler.parse("http://novikov.amikeco.ru/cvru.html").count(), 0L);
    }
}
