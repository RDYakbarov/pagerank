### Semester work for the course information retrieval ###
[task](http://novikov.amikeco.ru/iretrieve/pr-task.html)
---
In the folder [dist](https://github.com/Som3/pageRanker/tree/master/dist) is compiled Runner. First argument - page where to start <br/>
Runner support of comamds

 - ct - crawler threads (default 1)
 - cs - crawler store size (default 100)
 - c-sparse - use crawler store based on sparsed matrix
 - pt - page rank calculator threads (default 1)

Example

 - simple `java -jar PageRanker-1.0.jar http://novikov.amikeco.ru/`
 - full `java -jar PageRanker-1.0.jar http://novikov.amikeco.ru/ ct=3 cs=1000 c-sparse pt=5`